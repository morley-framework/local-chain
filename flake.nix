# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA
{
  description = "A local-chain flake";
  nixConfig.flake-registry = "https://gitlab.com/morley-framework/morley-infra/-/raw/main/flake-registry.json";
  inputs.xrefcheck.flake = false;

  outputs = inputs@{ self, nixpkgs, flake-utils, tezos-packaging, serokell-nix, xrefcheck, ... }: flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      pkgs = nixpkgs.legacyPackages.${system}.extend serokell-nix.overlay;
    in {
      checks = let
        run-check = name: deps: command: pkgs.runCommand "${name}-check" { buildInputs = deps; } (command + "\ntouch $out");
      in {
        trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;
        reuse-lint = pkgs.build.reuseLint ./.;
        black = run-check "black" [ pkgs.python39Packages.black ] ''
          black --check --diff --color .
        '';
        shellcheck = run-check "shellcheck" [ pkgs.shellcheck ] ''
          find . -name '*.sh' -exec shellcheck {} +
        '';
      };
      devShells = {
        default = pkgs.mkShell {
          buildInputs = with tezos-packaging.packages.${system}; [
            octez-client
            octez-node
          ] ++ builtins.attrValues (pkgs.lib.filterAttrs
            (name: attrs: !isNull (builtins.match "octez-baker-.*" name)) tezos-packaging.packages.${system});
        };
      };
      utils = {
        # TODO update when xrefcheck is flakified
        xrefcheck = import inputs.xrefcheck {};
      };
    }) // (let
      protocols = [ "parisc" "quebec" ];
    in {
      # JSONs with local-chain protocol parameters for currently supported protocols.
      # Note, these are not derivations and they aren't supposed to be used by 'nix build'.
      protocol-parameters = builtins.listToAttrs (builtins.map (proto: {
        name = "${proto}";
        value = builtins.fromJSON (builtins.readFile ./parameters-${proto}.json);
      }) protocols);
    });
}
