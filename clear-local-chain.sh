#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# Use this script to erase all data produced by running local chain.

set -euo pipefail
shopt -s extglob

if [[ "$#" -eq 0 ]];
then
    echo "You should provide base network for your local-chain as an argument, for example:"
    echo "./clear-local-chain.sh hangzhounet"
    exit 1
fi

base_chain="$1"

if [[ -f node-dir-$base_chain/chain_id ]]; then
    chain_id="$(cat "node-dir-$base_chain/chain_id")"
else
    chain_id="unset"
fi

local_chain_dir="$(cd "$(dirname "$0")" &> /dev/null && pwd)"

rm -rf "$local_chain_dir/node-dir-$base_chain"/!(identity.json)

DATA_DIR="$HOME/.tezos-client"

for f in "$DATA_DIR"/*; do
    # For some reason only prefix of the actual chain_id is used :shrug:
    if [[ $f =~ ${chain_id::13} ]]; then
        rm -rf "$f"
    fi
done
