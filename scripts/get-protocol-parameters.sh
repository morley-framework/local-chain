#!/bin/bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

###############################################################################
# Grab configurable constants from testnet node
#
# This script makes a request to `/context/constants/parametric` RPC endpoint,
# then tries to patch some values specific to local-chain. If constants we
# patch are renamed/removed, this will break.
###############################################################################

set -euo pipefail

# Stuff we want to patch, JSON.
# NB: blocks_per_epoch needs to be >= 1 and <= blocks_per_cycle.
OVERRIDES='{
    "blocks_per_cycle" : 8,
    "blocks_per_commitment" : 4,
    "nonce_revelation_threshold" : 4,
    "minimal_block_delay" : "1",
    "delay_increment_per_round" : "5",
    "bootstrap_accounts" : [
        [
            "edpkubXzL1rs3dQAGEdTyevfxLw3pBCTF53CdWKdJJYiBFwC1xZSct",
            "40000000000000"
        ]
    ]
}'

# Stuff we want to remove, Bash array of JSON paths.
TO_REMOVE=(
    "testnet_dictator"
    )

# Network name in XXXnet format, e.g. limanet.
NET="${1:-""}"

DEFAULT_NODE="https://<net>.testnet.tezos.serokell.team"

NODE_URI="${2:-${DEFAULT_NODE/<net>/${NET%net}}}"

# File in the repo root to write to.
OUTPUT="parameters-${NET%net}.json"

###############################################################################
# Main script; no configuration below this line
###############################################################################

if [[ "${NET}" == "${NET%net}" ]]; then
    cat << EOF
USAGE: $0 XXXnet

Example:
$0 limanet

If you need to override the node URI, pass it as the second parameter, e.g.

$0 limanet https://rpc.limanet.teztnets.xyz

The default is ${DEFAULT_NODE}
EOF
    exit 1
fi

self_dir="$(dirname "$0")"
cd "${self_dir}/.."

die() {
    echo "$1"
    exit 1
}

REMOVALS=""
for i in "${TO_REMOVE[@]}"; do
    REMOVALS="${REMOVALS} | del(.${i})"
done

constants="$(curl -f -s "${NODE_URI}/chains/main/blocks/head/context/constants/parametric")" \
    || die "Failed to get parameters from RPC node ${NODE_URI}"
patched="$(jq ". * ${OVERRIDES} ${REMOVALS}" <<< "${constants}")" || die "Failed to patch parameters"
echo "Writing to ${OUTPUT}"
cat <<< "${patched}" > "${OUTPUT}" || die "Failed to write parameters"
