<!--
SPDX-FileCopyrightText: 2021 Oxhead Alpha
SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# Run your own Tezos blockchain with just one node and baker.

## Prerequisites

You will need `octez-client`, `octez-node` and `octez-baker-<protocol>` binaries.
You can get them from [tezos-packaging repo](https://github.com/serokell/tezos-packaging).

Binaries from v16.0 or newer are required.

## Running local chain

Add `octez-node`, `octez-client` and `octez-baker-<protocol>` to your PATH.

If you're using `nix` you can use `nix develop` to enter the shell where
all required Octez binaries are added to the PATH

These two accounts will be used for our local chain:

* `baker`:
  ```
  Hash: tz1V8fDHpHzN8RrZqiYCHaJM9EocsYZch5Cy
  Public Key: edpkubXzL1rs3dQAGEdTyevfxLw3pBCTF53CdWKdJJYiBFwC1xZSct
  Secret Key: unencrypted:edsk47wWUSCHRDC1Hxtg7yxXzocwDjBJJRqTKnoP7htAifSpzwkg8K
  ```
* `genesis`:
  ```
  Hash: tz1hHGTh6Yk4k7d2PiTcBUeMvw6fJCFikedv
  Public Key: edpkvP4vq1PjEmfgfsiWpnQmojx4GYhW5hPHPfomWtmjdUULxRDjRt
  Secret Key: unencrypted:edsk3efmvuZ9dbhjRCEvfH47Ad3LmrZZgCadfYT6wmTgnN2E6XaEYh
  ```

Public key of `baker` account is used in `bootstrap_accounts` in the protocol `parameters` file,
see one of protocol specific parameters files.
This file also specifies another chain parameters, e.g. `time_between_blocks` specifies
how often blocks will be produced, we use as small amount as possible in order to be able
to quickly perform huge amount of operations on the chain.
In case you don't fit gas limit, you can increase it by increasing
`hard_gas_limit_per_operation` and `hard_gas_limit_per_block`

You may need to download zcash params in case you haven't done it previously.
In order to download them run:
(note, that it will consume ~800MB of your disk space):
```
curl -s https://raw.githubusercontent.com/zcash/zcash/master/zcutil/fetch-params.sh | bash
```

Now run [`run-local-chain.sh`](./run-local-chain.sh) script. This script will add `genesis`
and `baker` accounts to your `octez-client`. Apart from that it will start sandboxed
node on `localhost:8732` with data stored in `node-dir-<base-network-name>` along with baker daemon (in backround).
This script accepts base network as an argument, e.g. `./run-local-chain.sh kathmandunet`.

Now you have local chain with node on `localhost:8732` which can be used for testing purposes.

Transfer some tokens to your `rich` account:
```sh
octez-client -E http://localhost:8732 transfer 10000 from baker to rich --burn-cap 0.257
```

### **Tenderbake local-chain safety disclaimer**

Keep in mind that Tenderbake (which is used on Ithaca and following protocols) needs 2/3 of delegates
(stake wise) to be active (running a baker) to allow the chain to progress. This means that you shouldn't
transfer more than 2/3 of the `baker` stake to addresses that aren't running a baker.

It's possible to run local-chain in the background. Set the `LOCAL_CHAIN_IN_BACKGROUND=true`
before starting `run-local-chain.sh` in order to launch all binaries in the background.
In order to stop local-chain running in the background, you may kill the `octez-node` process:
`pkill octez-node`.

## Restart local chain

Once you finished playing with your local chain, you can kill `run-local-chain.sh`
and restart it once again when you need it.

## Reset local chain

If you want to reset the blockchain, just nuke `node-dir` directory, and remove
`blocks`, `nonces` and `wallet_lock` files from your `octez-clients`'s `base-dir`
(`$HOME/.tezos-client` by default).
You can use the [`clear-local-chain.sh`](./clear-local-chain.sh) script to do that.
This script also accepts base network name as an argument to determine node data
directory that needs to be cleaned. E.g. `./clear-local-chain.sh kathmandunet`.

## Additional notes on `octez-client` config.

If you want to run multiple different local chains, you should use different `base_dir`s for
`octez-client`, you can set up `base_dir` in the `octez-client` config file.
Otherwise, you'll encounter baker errors. Note, that public/secret keys
and public keys hashes are stored in the `base_dir`, so you'll need to import `genesis` and
`baker` keys for each `base_dir`.

If you encounter baker errors remove `blocks`, `nonces`, `wallet_lock` files from
your `base-dir`

## Expose your local network to the outer world

Sometimes you want other people to access you node remotely (for example, via VPN).
In order to do it set `MORLEY_LOCALNET` environment variable to the network address of your computer.

# Testing scenarios

Sometimes it's required to test how different software works in some specific scenarios
that may happen on Tezos blockchain.

## Voting scenario

Running this scenario requires Python with version at least 3.7. Also `octez-client` and `octez-node` binaries
are expected to be in `PATH`.

[voting.py](./scenarios/voting.py) script provides an ability to test the voting during the Tezos amendment process.
This scenario goes over the voting process, allows to vote via arbitrary software, and provides a config
for the `octez-node` that can be used to connect other nodes to the `local-chain` that is used during this voting test scenario.

Usage example:
```
./scenarios/voting.py --base-protocol kathmandu
```
