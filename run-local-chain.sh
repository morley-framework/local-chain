#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2021-2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

set -euo pipefail

function wait_for_node_to_start () {
  while ! _="$(curl --silent http://"$MORLEY_LOCALNET":8732/chains/main/blocks/head/)"; do
    echo "octez-client try to connect to node"
    sleep 1s
  done
}

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER="Y"

local_chain_dir="$(cd "$(dirname "$0")" &> /dev/null && pwd)"

MORLEY_LOCALNET=${MORLEY_LOCALNET:-localhost}

base_chain="${1:-pariscnet}"
protocol=""
additional_baker_options=("--liquidity-baking-toggle-vote" "pass")
if [[ $base_chain = "quebecnet" ]]; then
    baker_binary="octez-baker-PsQuebec"
    protocol="PsQuebecnLByd3JwTiGadoG4nGWi3HYiLXUjkibeFV8dCFeVMUg"
elif [[ $base_chain = "pariscnet" ]]; then
    baker_binary="octez-baker-PsParisC"
    protocol="PsParisCZo7KAh1Z1smVd9ZMZ1HHn5gkzbM94V3PLCpknFWhUAi"
else
    echo "$base_chain is currently not supported. Currently supported base networks: 'quebecnet', 'pariscnet'"
    exit 1
fi

node_dir="$local_chain_dir/node-dir-$base_chain"

mkdir -p "$node_dir"

genesis_secret_key="unencrypted:edsk3efmvuZ9dbhjRCEvfH47Ad3LmrZZgCadfYT6wmTgnN2E6XaEYh"
required_genesis_addr="tz1hHGTh6Yk4k7d2PiTcBUeMvw6fJCFikedv"
baker_secret_key="unencrypted:edsk47wWUSCHRDC1Hxtg7yxXzocwDjBJJRqTKnoP7htAifSpzwkg8K"
required_baker_addr="tz1V8fDHpHzN8RrZqiYCHaJM9EocsYZch5Cy"

if ! genesis_addr="$(octez-client list known addresses | grep "genesis:" | cut -d " " -f 2)"; then
    echo "Adding genesis key"
    octez-client import secret key genesis "$genesis_secret_key"
elif [[ $genesis_addr != "$required_genesis_addr" ]]; then
    echo "$genesis_addr doesn't match required 'genesis' address: $required_genesis_addr"
    exit 1
fi

if ! baker_addr="$(octez-client list known addresses | grep "baker:" | cut -d " " -f 2)"; then
    echo "Adding baker key"
    octez-client import secret key baker "$baker_secret_key"
elif [[ $baker_addr != "$required_baker_addr" ]]; then
    echo "$baker_addr doesn't match required 'baker' address: $required_baker_addr"
    exit 1
fi

[[ -f $node_dir/identity.json ]] ||
    (mkdir -p "$node_dir"; octez-node "identity" "generate" "1" "--data-dir" "$node_dir")

( wait_for_node_to_start &&
echo "octez-client connected to the node"
if [[ "$(octez-client -E "http://$MORLEY_LOCALNET:8732" rpc get "/chains/main/blocks/head/" \
    | jq -r '.header.level')" -eq 0 ]]; then
    parameters_file="$local_chain_dir/parameters-${base_chain%net}.json"
    echo "Activate $base_chain";
    octez-client --block genesis --endpoint "http://$MORLEY_LOCALNET:8732" activate protocol \
      "$protocol" with fitness 0 \
      and key genesis and parameters "$parameters_file"
else
    echo "Protocol is already activated";
fi ) &

(wait_for_node_to_start &&
echo "Starting baker daemon"
# Since ithaca protocol chain_id is used in directories names in octez-client data directory
octez-client compute chain id from block hash "$(jq -r .network.genesis.block < "$node_dir/config.json")" > "$node_dir/chain_id"
"$baker_binary" --endpoint "http://$MORLEY_LOCALNET:8732" run \
  with local node "$node_dir" baker &>"$local_chain_dir/baker.log" "${additional_baker_options[@]}") &
cat > "$node_dir/config.json" <<- EOM
{
  "p2p": {},
  "network": {
    "genesis": {
      "timestamp": "2021-05-21T15:00:00Z",
      "block": "BMFCHw1mv3A71KpTuGD3MoFnkHk9wvTYjUzuR9QqiUumKGFG6pM",
      "protocol": "ProtoGenesisGenesisGenesisGenesisGenesisGenesk612im"
    },
    "genesis_parameters": {
      "values": {
        "genesis_pubkey": "edpkvP4vq1PjEmfgfsiWpnQmojx4GYhW5hPHPfomWtmjdUULxRDjRt"
      }
    },
    "chain_name": "TEZOS_LOCALNET_2021-05-21T15:00:00Z",
    "old_chain_name": "TEZOS_LOCALNET_2021-05-21T15:00:00Z",
    "incompatible_chain_name": "INCOMPATIBLE",
    "sandboxed_chain_name": "SANDBOXED_TEZOS",
    "default_bootstrap_peers": []
  }
}
EOM

echo "Starting octez-node"
if [[ -z ${LOCAL_CHAIN_IN_BACKGROUND+x} ]]; then
  octez-node run --data-dir "$node_dir" --rpc-addr "$MORLEY_LOCALNET:8732" \
    --bootstrap-threshold 0 --no-bootstrap-peers &>"$local_chain_dir/node.log"
else
  (octez-node run --data-dir "$node_dir" --rpc-addr "$MORLEY_LOCALNET:8732" \
    --bootstrap-threshold 0 --no-bootstrap-peers &>"$local_chain_dir/node.log") &
  wait_for_node_to_start
fi
